from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),
    path('register', views.register, name='register'),
    path('query', views.query, name='query'),
    path('add', views.add, name='add'),
    path('vote', views.vote, name='vote'),
    path('tick', views.tick, name='tick'),
    #path('import', views.importer, name='import'),
]