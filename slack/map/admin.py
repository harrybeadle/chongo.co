from django.contrib import admin

from .models import Slackline, Slacker

admin.site.register(Slackline)
admin.site.register(Slacker)
