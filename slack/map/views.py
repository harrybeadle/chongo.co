from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import auth
from django.core import serializers

from .models import Slackline, Slacker
from .forms import SlacklineForm

def index(request, **kwargs):
    context = {}
    context["slacklines"] = Slackline.objects.order_by('-created_on')
    context["activity"] = context["slacklines"][:20]
    if "login_failed" in request.session:
        context["login_failed"] = request.session["login_failed"]
        request.session["login_failed"] = False
    else:
        context["login_failed"] = False
    return render(request, "map/index.html", context)

def login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = auth.authenticate(request, username=username, password=password)
    if user is not None:
        auth.login(request, user)
        return redirect(index)
    else:
        request.session["login_failed"] = True
        return redirect(index)

def logout(request):
    auth.logout(request)
    return redirect(index)

def register(request):
    username = request.POST['username']
    password = request.POST['password']
    email = request.POST['email']
    user = auth.models.User.objects.create_user(username=username, 
                                                password=password, 
                                                email=email)
    user.save()
    auth.login(request, user)
    return redirect(index)

def query(request):
    if "lines" in request.GET:
        lines = Slackline.objects.order_by('-created_on')
        return HttpResponse(serializers.serialize('json', lines))
    if "username" in request.GET:
        try:
            user = auth.models.User.objects.get(username=request.GET["username"])
            return HttpResponse("1")
        except auth.models.User.DoesNotExist:
            return HttpResponse("0")
    if "email" in request.GET:
        try: 
            email = auth.models.User.objects.get(email=request.GET["email"])
            return HttpResponse("1")
        except auth.models.User.DoesNotExist:
            return HttpResponse("0")
    if "user" in request.GET:
        try:
            if request.GET["user"]:
                user = auth.models.User.objects.get(username=request.GET["user"])
            else:
                user = request.user
            try: 
                slacker = Slacker.objects.get(user=user.id)
            except Slacker.DoesNotExist:
                slacker = Slacker.objects.create(user=user)
            return HttpResponse(serializers.serialize('json', [slacker]))
        except:
            pass
    return HttpResponse("")

def add(request):
    upvotes = 0
    downvotes = 0
    if "delete_by_uid" in request.POST:
        uid = request.POST["delete_by_uid"]
        try:
            to_delete = Slackline.objects.get(uid=uid)
            upvotes = to_delete.upvotes
            downvotes = to_delete.downvotes
            if to_delete.owner == request.user.username:
                to_delete.delete()
        except:
            pass

    form = SlacklineForm(request.POST)
    if form.is_valid(): 
        record = form.save(commit=False)
        record.owner = request.user.username
        record.upvotes = upvotes
        record.downvotes = downvotes
        record.save()
    else:
        return HttpResponse(form.errors)
    return redirect(index)

def vote(request):
    try: 
        slacker = Slacker.objects.get(user=request.user.id)
    except Slacker.DoesNotExist:
        slacker = Slacker.objects.create(user=request.user)
    try: 
        line = Slackline.objects.get(uid=request.POST["uid"])
        if "upvote" in request.POST:
            line.upvotes += 1
            slacker.upvotes.append(line.uid)
            slacker.save()
            line.save()
            return HttpResponse("upvote")
        if "downvote" in request.POST:
            line.downvotes += 1
            slacker.downvotes.append(line.uid)
            slacker.save()
            line.save()
            return HttpResponse("downvote")
    except:
        return HttpResponse("error")

def tick(request):
    try:
        slacker = Slacker.objects.get(user=request.user.id)
    except Slacker.DoesNotExist:
        slacker = Slacker.objects.create(user=request.user)
    slacker.ticks.append(request.POST["uid"])
    slacker.save()
    try: 
        line = Slackline.objects.get(uid=request.POST["uid"])
        line.ticks += 1
        line.save()
    except:
        pass
    return HttpResponse("tick")

def importer(request):
    return render(request, "map/import.html")