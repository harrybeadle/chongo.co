from django.forms import ModelForm, ChoiceField

from .models import *

class SlacklineForm(ModelForm):
    class Meta:
        model = Slackline
        fields = [
            "name",
            "linetype",
            "notes",
            "coordinates",
            "length",
            "locality",
            "country_code",
        ]