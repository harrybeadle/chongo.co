class UserDialog extends GenericDialog {
    constructor(username) {
        super();
        this.title.innerHTML = `${username}'s Profile`;
        // Tab Navigator
        this.tabNav = this.body.appendChild(document.createElement("nav"));
        this.tabNav.className = "nav nav-pills nav-justified";
        this.tabNav.id = "user-tabs";
        this.tabButtons = {};
        ["Lines", "Likes", "Sends"].forEach((tabName, index) => {
            // Tab Button
            let div = this.tabNav.appendChild(document.createElement("btn"));
            div.className = "btn nav-item nav-link";
            if (index == 0) {
                div.className += " active";
            }
            div.innerHTML = tabName;
            div.addEventListener("click", () => {
                this.showTab(tabName);
            })
            this.tabButtons[tabName] = div;

        });
        // Tab Content
    }

    showTab(tabName) {
        for (let name in this.tabButtons) {
            if (name != tabName) {
                this.tabButtons[name].classList.remove("active")
                this.tabContents[name].classList.push("hide")
            }
        }
    }
}