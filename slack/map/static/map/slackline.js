/*

Edit Dialog

*/

var editor;

class Editor {

    constructor() {
        this.anchors = []
        this.midpoint = null
        this.line = null
        this.length = null
        this.geocoder = new mapkit.Geocoder()
        this.location = null
        this.enabled = false

        this.anchorFactory = function(coordinate, options) {
            var div = document.createElement("div");
            div.className = "anchor";
            return div;
        }

        this.midpointFactory = function(coordinate, options) {
            var div = document.createElement("div");
            div.className = "midpoint";
            div.innerHTML = editor.length + "m";
            return div;
        }

        $('#map').click(this.addAnchorByClick)
        $('#edit-reset-anchors').click(this.resetAnchors)
        $('#edit-close-button').click(this.closeDialog)
        $('#edit-close-x').click(this.closeDialog)
        $('#edit-finish-anchors').click(this.detailMode)
        $('#edit-move-anchors').click(this.moveMode)
        $('#edit-init-anchors').click(this.detailMode)
        $('#edit-init-anchors').prop("disabled", true)
        this.anchorMode()
    }

    addAnchorByCoord(coord) {
        // Create an annotation at that point, adding it to `anchors`
        if (editor.anchors.length < 2) {
            let anchor = new mapkit.Annotation(
                coord, editor.anchorFactory, {
                    draggable: true
                }
            )
            anchor.addEventListener("dragging", event => {
                event.target._impl._coordinate = event.coordinate
                this.updateLine.call(this)
                this.updateMidpoint.call(this)
            })
            anchor.addEventListener("drag-end", event => {
                this.updateLocation.call(this);
            })
            editor.anchors.push(anchor)
            map.addAnnotation(anchor)
        }
        if (editor.anchors.length == 2) {
            editor.updateLine()
            editor.updateMidpoint()
            editor.updateLocation()
            editor.updateCoordinates()
            $('#anchor-counter').addClass("badge-secondary").removeClass("badge-danger")
            $('#edit-init-anchors').prop("disabled", false)
        } else {
            $('#anchor-counter').addClass("badge-danger").removeClass("badge-secondary")
            $('#edit-init-anchors').prop("disabled", true)
        }
        $('#anchor-count').html(String(editor.anchors.length))
    }

    addAnchorByClick(event) {
        if (!editor.enabled) return;
        // Find the place the click took place.
        var click = new DOMPoint(event.pageX, event.pageY);
        // Get the coordinate at that position.
        var coord = map.convertPointOnPageToCoordinate(click);
        editor.addAnchorByCoord(coord)
    }

    updateLocation() {
        if (editor.midpoint) {
            editor.geocoder.reverseLookup(
                editor.midpoint.coordinate,
                function(error, data) {
                    var result = data.results[0];
                    console.log(result)
                    if (!result.locality) {
                        result.locality = result.name
                        if (!result.name) {
                            result.locality = "Unknown"
                        }
                    }
                    if (!result.countryCode) {
                        result.countryCode = "??"
                    }
                    editor.location = result.locality + ", " + result.countryCode;
                    $('#edit-locality').val(result.locality)
                    $('#edit-country-code').val(result.countryCode)
                    $('#edit-location').val(editor.location);
                }
            )
        }
    }

    removeMidpoint() {
        if (editor.midpoint) {
            map.removeAnnotation(editor.midpoint)
        }
    }

    updateCoordinates() {
        $('#edit-coordinates').val(JSON.stringify({
            anchors: editor.anchors.map(function(annotation) {return annotation.coordinate}),
            midpoint: editor.midpoint.coordinate
        }))
    }

    calcLength() {
        const R = 6371e3; // metres
        const φ1 = editor.anchors[0].coordinate.latitude * Math.PI/180; // φ, λ in radians
        const φ2 = editor.anchors[1].coordinate.latitude * Math.PI/180;
        const Δφ = (editor.anchors[1].coordinate.latitude-editor.anchors[0].coordinate.latitude) * Math.PI/180;
        const Δλ = (editor.anchors[1].coordinate.longitude-editor.anchors[0].coordinate.longitude) * Math.PI/180;
        const A = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
                Math.cos(φ1) * Math.cos(φ2) *
                Math.sin(Δλ/2) * Math.sin(Δλ/2);
        const c = 2 * Math.atan2(Math.sqrt(A), Math.sqrt(1-A));
        const d = R * c; // in metres
        editor.length = Math.round(d)
        $('#edit-length').val(editor.length)
        return editor.length
    }

    updateMidpoint() {
        editor.calcLength()
        editor.removeMidpoint()
        var coord = new mapkit.Coordinate(
            (editor.anchors[0].coordinate.latitude + editor.anchors[1].coordinate.latitude) / 2,
            (editor.anchors[0].coordinate.longitude + editor.anchors[1].coordinate.longitude) / 2
        )
        editor.midpoint = new mapkit.Annotation(
            coord, editor.midpointFactory
        )
        map.addAnnotation(editor.midpoint)
    }

    removeLine() {
        if (editor.line) {
            map.removeOverlay(editor.line)
            editor.line = null
        }
    }

    updateLine() {
        editor.removeLine()
        editor.line = new mapkit.PolylineOverlay([
                editor.anchors[0].coordinate,
                editor.anchors[1].coordinate
            ],{ style: new mapkit.Style({
                strokeColor: "#000",
                lineWidth: 3,
                lineCap: "round",
                lineDash: [5, 5]
            })})
        map.addOverlay(editor.line)
    }

    resetAnchors() {
        // Reset the anchor init button
        $('#edit-init-anchors').prop("disabled", true)
        // Remove anchors from map.
        editor.anchors.forEach(function(item, index) {
            map.removeAnnotation(item)
        })
        // Reset the anchors
        editor.anchors = []
        $('#anchor-count').html(String(editor.anchors.length))
        // Remove the line from the map.
        if (editor.line) {
            map.removeOverlay(editor.line)
        }
        editor.line = null
        // Remove the midpoint marker
        if (editor.midpoint) {
            map.removeAnnotation(editor.midpoint)
        }
        editor.midpoint = null
        // Reset location
        editor.location = null
        // Return to set anchor view.
        editor.anchorMode()
    }

    anchorMode() {
        $('#edit-set-dialog').css("display", "block");
        $('#edit-detail-dialog').css("display", "none");
        $('#edit-move-dialog').css("display", "none");
        $('#edit-footer').css("display", "none");
    }

    detailMode() {
        $('#edit-set-dialog').css("display", "none");
        $('#edit-detail-dialog').css("display", "block");
        $('#edit-move-dialog').css("display", "none");
        $('#edit-footer').css("display", "block");
    }

    moveMode() {
        $('#edit-set-dialog').css("display", "none");
        $('#edit-detail-dialog').css("display", "none");
        $('#edit-move-dialog').css("display", "block");
        $('#edit-footer').css("display", "none");
    }

    closeDialog() {
        editor.resetAnchors()
        editor.anchorMode()
        editor.enabled = false;
        $('#edit-init-anchors').prop("disabled", true)
        $('#edit-modal').modal('hide')
        $("#edit-to-delete").val("")
        editor.editing.addToMap()
    }

    edit(uid) {
        editor.enabled = true;
        $('#edit-modal').modal();
        editor.editing = slacklines[uid]
        editor.editing.removeFromMap()
        $('#edit-name').val(editor.editing.fields.name)
        $('#edit-linetype').val(editor.editing.fields.linetype)
        $('#edit-notes').val(editor.editing.fields.notes)
        $('#edit-length').val(editor.editing.fields.length)
        $('#edit-coordinates').val(editor.editing.fields.coordinates)
        $('#edit-location').val(`${editor.editing.fields.locality}, ${editor.editing.fields.country_code}`)
        editor.detailMode();
        editor.addAnchorByCoord(new mapkit.Coordinate(
            editor.editing.fields.coordinates.anchors[0].latitude,
            editor.editing.fields.coordinates.anchors[0].longitude
        ))
        editor.addAnchorByCoord(new mapkit.Coordinate(
            editor.editing.fields.coordinates.anchors[1].latitude,
            editor.editing.fields.coordinates.anchors[1].longitude
        ))
        $("#edit-to-delete").val(editor.editing.fields.uid)
    }

}

editor = new Editor();

$('#add-button').click(function() {
    editor.enabled = true;
    editor.anchorMode()
    $('#edit-modal').modal();
})

/*

Loading in Lines

*/

function lineColor(linetype) {
    if (linetype == "Longline") return "#c45ab3";
    if (linetype == "Waterline") return "#3c91e6";
    if (linetype == "Slackline") return "#a8c686";
    if (linetype == "Highline") return "#e4572e";
    return "cyan";
}

class Slackline {

    constructor(fields) {
        this.annotation = null
        this.overlay = null

        this.factory = function(coordinate, options) {
            var div = document.createElement("div");
            div.id = options.data.uid
            div.className = "slackline-marker " + options.data.linetype;
            div.innerHTML = options.data.length + "m";
            return div;
        }

        this.fields = fields
        this.color = lineColor(this.fields.linetype)
        this.anchors = [
            new mapkit.Coordinate(
                this.fields.coordinates.anchors[0].latitude,
                this.fields.coordinates.anchors[0].longitude
            ),
            new mapkit.Coordinate(
                this.fields.coordinates.anchors[1].latitude,
                this.fields.coordinates.anchors[1].longitude
            )
        ]
        this.midpoint = new mapkit.Coordinate(
            this.fields.coordinates.midpoint.latitude,
            this.fields.coordinates.midpoint.longitude
        )
        this.addToMap()
    }

    addToMap() {
        if (!this.overlay) {
            this.overlay = new mapkit.PolylineOverlay(
                this.anchors,
                {style: new mapkit.Style({
                    strokeColor: this.color,
                    lineWidth: 5,
                    lineCap: "round"
                })}
            )
            this.overlay.addEventListener("select", () => {
                new InfoDialog(this).show();
            })
            map.addOverlay(this.overlay)
        }
        if (!this.annotation) {
            this.annotation = new mapkit.Annotation(
                this.midpoint,
                this.factory,
                {
                    data: this.fields,
                    clusteringIdentifier: "slackline"
                }
            )
            this.annotation.addEventListener("select", event => {
                // Show the Dialog Box
                new InfoDialog(this).show();
                // After a quick delay deselect the annotation
                setTimeout(() => {
                    this.annotation.selected = false;
                }, 100);
            })
            map.addAnnotation(this.annotation)
        }
    }

    removeFromMap() {
        if (this.annotation) {
            map.removeAnnotation(this.annotation)
            this.annotation = null
        }
        if (this.overlay) {
            map.removeOverlay(this.overlay)
            this.overlay = null
        }
    }

    share() {
        let dialog = new GenericDialog();
        // Title
        dialog.title.innerHTML = "Share by Link";
        // Body
        dialog.body.innerHTML = "This link will go straight to the information for this line.";
        let group = dialog.body.appendChild(document.createElement("div"));
        group.className = "input-group mt-3";
        let input = group.appendChild(document.createElement("input"));
        input.disabled = true;
        input.className = "form-control";
        input.value = "https://chongo.co/?uid=" + this.fields.uid;
        let append = group.appendChild(document.createElement("div"));
        append.className = "input-group-append";
        let copy = append.appendChild(document.createElement("button"));
        copy.className = "btn btn-secondary";
        copy.innerHTML = "<i class=\"icon\">content_copy</i>";
        copy.addEventListener("click", event => {
            navigator.clipboard.writeText(input.value);
        });
        // Footer
        dialog.footer.style.display = "none";
        dialog.show();
        return dialog;
    }

    edit() {
        editor.edit(this.fields.uid);
    }

    delete() {
        $.post("add", {
            "delete_by_uid": this.fields.uid,
            csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val()
        }, ()  => {
            window.location.reload(true);
        })
    }
}

var slacklines = {}

//
// Get Lines from Server
//

$.get("query?lines", data => {
    // Parse JSON, adding all slacklines to the global slackline list.
    data = JSON.parse(data)
    data.forEach(item => {
        slacklines[item.fields.uid] = new Slackline(item.fields)
    })
    // Add these slacklines to the activity dialog.
    ad.addDict(slacklines)
    // Check the URL, if the URL has a UID, show that slackline's dialog.
    var split = window.location.href.split("?uid=")
    if (split.length == 2) {
        uid = split[1];
        new InfoDialog(slacklines[uid]).show();
    }
});

class ActivityDialog {
    constructor() {
        // Modal
        this.modal = document.createElement("div")
        this.modal.className = "modal modal-slack"
        // Dialog
        this.dialog = this.modal.appendChild(document.createElement("div"))
        this.dialog.className = "modal-dialog modal-dialog-scrollable m-1"
        // Content
        this.content = this.dialog.appendChild(document.createElement("div"))
        this.content.className = "modal-content"
        // Header
        this.header = this.content.appendChild(document.createElement("div"))
        this.header.className = "modal-header"
        // Title
        this.title = this.header.appendChild(document.createElement("span"))
        this.title.className = "modal-title"
        this.title.innerHTML = "Slackline Activity"
        // X Button
        this.xButton = this.header.appendChild(document.createElement("button"))
        this.xButton.className = "close"
        this.xButton.innerHTML = "&times;"
        this.xButton.addEventListener("click", event => {
            $(this.modal).modal("hide")
        })
        // Body
        this.body = this.content.appendChild(document.createElement("div"))
        this.body.className = "modal-body"
        // Selector
        this.selectorGroup = this.body.appendChild(document.createElement("div"))
        this.selectorGroup.className = "form-inline w-100 mb-2"
        this.selectorLabel = this.selectorGroup.appendChild(document.createElement("label"))
        this.selectorLabel.className = "mr-2"
        this.selectorLabel.innerHTML = "Sort by"
        this.selector = this.selectorGroup.appendChild(document.createElement("select"))
        this.selector.className = "custom-select flex-grow-1";
        ["New", "Best", "Popular"].map(value => {
            let option = this.selector.appendChild(document.createElement("option"))
            option.value = value;
            option.innerHTML = value;
        })
        this.selector.addEventListener("change", event => {
            this.sort.call(this);
        })
        // List
        this.list = this.body.appendChild(document.createElement("ul"))
        this.list.className = "list-group"
        // List of Objects displayed in the List Group 
        this.items = []
        return this
    }

    addListItem(slackline) {
        if (this.items.indexOf(slackline) < 0) {
            this.items.push(slackline)
        }
        // List Item
        let item = this.list.appendChild(document.createElement("li"))
        item.className = "list-group-item list-group-item-action"
        item.addEventListener("click", event => {
            new InfoDialog(slackline).show();
            this.hide();
        });
        // Length Badge
        let badge = item.appendChild(document.createElement("span"))
        badge.className = `badge ${slackline.fields.linetype}`
        badge.innerHTML = `${slackline.fields.length}m`
        // Linetype
        let type = item.appendChild(document.createElement("span"))
        type.innerHTML = ` ${slackline.fields.linetype}`
        // Location
        let location = item.appendChild(document.createElement("span"))
        location.className = "text-secondary"
        location.innerHTML = ` in ${slackline.fields.locality}`
        item.appendChild(document.createElement("br"))
        // Details
        let details = item.appendChild(document.createElement("small"))
        if (slackline.fields.name) {
            details.innerHTML += `"${slackline.fields.name}" `
        }
        details.innerHTML += `by ${slackline.fields.owner}`
        // Flag
        let flag = item.appendChild(document.createElement("span"))
        flag.className = `float-right flag-icon flag-icon-${slackline.fields.country_code}`
        // Voting
        let voteBar = item.appendChild(document.createElement("div"))
        voteBar.className = "progress"
        voteBar.style = "height: 2px;"
        let upvoteBar = voteBar.appendChild(document.createElement("div"))
        upvoteBar.className = "progress-bar bg-success"
        let downvoteBar = voteBar.appendChild(document.createElement("div"))
        downvoteBar.className = "progress-bar bg-danger"
        let upvotePercent = slackline.fields.upvotes
        upvotePercent /= (slackline.fields.upvotes + slackline.fields.downvotes)
        upvotePercent *= 100
        upvotePercent = Math.round(upvotePercent)
        if (slackline.fields.upvotes + slackline.fields.downvotes == 0) {
            upvoteBar.style = "width: 50%";
            downvoteBar.style = "width: 50%";
        } else {
            upvoteBar.style = `width: ${upvotePercent}%;`
            downvoteBar.style = `width: ${100 - upvotePercent}%;`
        }

    }

    addDict(dict) {
        Object.keys(dict).map(key => slacklines[key]).forEach(item => {
            this.addListItem(item)
        })
    }

    addList(list) {
        list.map(item => {this.addListItem(item)})
    }

    clear() {
        while (this.list.firstChild) {
            this.list.removeChild(this.list.firstChild)
        }
    }

    show() {
        $(this.modal).modal({
            backdrop: false,
            keyboard: true,
            focus: true,
            show: true
        })
    }

    hide() {
        $(this.modal).modal("hide")
    }

    sort() {
        let method = this.selector.value;
        if (method == "New") {
            this.items.sort((a, b) => {
                return ((new Date(b.fields.created_on).getTime()) > (new Date(a.fields.created_on).getTime()))
            })
        } else if (method == "Best") {
            this.items.sort((a, b) => {
                let apcent, bpcent;
                if (a.fields.upvotes + a.fields.downvotes == 0) {
                    apcent = 0;
                } else {
                    apcent = a.fields.upvotes / (a.fields.upvotes + a.fields.downvotes)
                }
                if (b.fields.upvotes + b.fields.downvotes == 0) {
                    bpcent = 0;
                } else {
                    bpcent = b.fields.upvotes / (b.fields.upvotes + b.fields.downvotes)
                }
                return bpcent - apcent
            })
        } else if (method == "Popular") {
            this.items.sort((a, b) => {
                return (b.fields.upvotes + b.fields.downvotes) - (a.fields.upvotes + a.fields.upvotes)
            })
        }
        this.clear()
        this.addList(this.items)
    }

}

var ad = new ActivityDialog()

