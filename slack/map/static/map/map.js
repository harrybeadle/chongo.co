//
// Map
//

// Initialize with Token from Apple
const tokenID = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IkREWTQ3NU1YSDgifQ.eyJpc3MiOiJIMkZBNzk2NDM5IiwiaWF0IjoxNTkxMzU4NzA1LCJleHAiOjE2MjI1MDU2MDB9.jIebEdvR6Zi-CV27TKixhojXstT1EhZgLqCD5UAan5G3CvCrKX9SAhn0AUeO9mMN1-stKz3YrdBJ2ZEVSEz-Kw";
mapkit.init({
    authorizationCallback: function(done) {
        done(tokenID);
    }
});

// Create the Map and set some basic GUI settings.
map = new mapkit.Map("map");
map._allowWheelToZoom = true;
map.isRotationEnabled = false;
map.showsCompass = mapkit.FeatureVisibility.Hidden
map.showsMapTypeControl = false;
map.showsPointsOfInterest = false;
map.showsScale = mapkit.FeatureVisibility.Visible;

// Store the viewport in a cookie.
map.addEventListener("region-change-end", function () {
    document.cookie = "center=" + JSON.stringify(map.center) + "; SameSite=Strict"
    document.cookie = "zoom=" + String(map.cameraDistance) + "; SameSite=Strict"
})

// When this is first run set the viewport back to that which is stored in the cookie.
document.cookie.split("; ").forEach(function (item, index) {
    cookie = item.split("=")
    if (cookie[0] == "center") {
        pin = JSON.parse(cookie[1])
        pin = new mapkit.Coordinate(pin.latitude, pin.longitude)
        map.setCenterAnimated(pin, false)
    }
    if (cookie[0] == "zoom") {
        map.cameraDistance = cookie[1]
    }
})

// Change Map to Hybrid Style when zoomed in.
function scale_handle(event) {
    // Map Type
    if (map.cameraDistance < 8000) {
        map.mapType = mapkit.Map.MapTypes.Hybrid;
    } else {
        map.mapType = mapkit.Map.MapTypes.Standard;
    }
    // Clustering
    if (map.cameraDistance < 2000) {
        map.annotations.forEach(function(item, index) {
            item.clusteringIdentifier = String(Math.random())
        })
    } else {
        map.annotations.forEach(function(item, index) {
            item.clusteringIdentifier = "slackline"
        })
    }
}
map.addEventListener("zoom-end", scale_handle);
setTimeout(scale_handle, 100)