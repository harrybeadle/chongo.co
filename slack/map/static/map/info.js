class LineTitle {
    constructor(slackline) {
        this.div = document.createElement("div");
        // Length Badge
        this.badge = this.div.appendChild(document.createElement("div"));
        this.badge.className = `badge ${slackline.fields.linetype}`;
        this.badge.innerHTML = `${slackline.fields.length}m`;
        // Linetype
        this.linetype = this.div.appendChild(document.createElement("span"));
        this.linetype.innerHTML = ` ${slackline.fields.linetype} `;
        // Location
        this.location = this.div.appendChild(document.createElement("span"));
        this.location.className = "text-secondary";
        this.location.innerHTML = `in ${slackline.fields.locality} `;
        // Flag
        this.flag = this.location.appendChild(document.createElement("span"))
        this.flag.className = `flag-icon flag-icon-${slackline.fields.country_code}`
        // Line Break
        this.div.appendChild(document.createElement("br"));
        // Name and Owner
        this.details = this.div.appendChild(document.createElement("small"));
        if (slackline.fields.name) {
            this.details.innerHTML += `"${slackline.fields.name}" `;
        }
        this.details.innerHTML += `by ${slackline.fields.owner}`;
    }
}

class LineToolbar {
    constructor(slackline, parent=null) {
        this.parent = parent
        this.toolbar = document.createElement("div");
        this.toolbar.className = "btn-toolbar w-100";
        // Owner Tools
        let username = document.getElementById("username").innerHTML.trim();
        if (slackline.fields.owner == username) {
            this.ownerTools = this.toolbar.appendChild(document.createElement("div"));
            this.ownerTools.className = "btn-group mr-2 flex-grow-1";
            // Edit Button 
            this.editButton = this.ownerTools.appendChild(document.createElement("button"));
            this.editButton.appendChild(iconSpan("edit", true))
            this.editButton.className = "btn btn-secondary"
            this.editButton.addEventListener("click", () => {
                parent.hide();
                slackline.edit();
            })
            // Delete Button
            this.deleteButton = this.ownerTools.appendChild(document.createElement("button"));
            this.deleteButton.appendChild(iconSpan("delete_forever", true))
            this.deleteButton.className = "btn btn-secondary"
            this.deleteButton.addEventListener("click", () => {
                if (!this.deleteButton.classList.contains("btn-danger")) {
                    this.deleteButton.classList.add("btn-danger");
                    while (this.deleteButton.firstChild) {
                        this.deleteButton.removeChild(this.deleteButton.firstChild);
                    }
                    this.deleteButton.appendChild(iconSpan("delete_forever", "Are you sure?"));
                } else {
                    slackline.delete();
                }
            });
        }
        // General Tools
        this.generalTools = this.toolbar.appendChild(document.createElement("div"))
        this.generalTools.className = "btn-group mr-2 flex-grow-1";
        // Sent Button
        this.sentButton = this.generalTools.appendChild(document.createElement("button"));
        this.sentButton.appendChild(iconSpan("done", "Sent it!"));
        this.sentButton.className = "btn btn-secondary"
        this.sentButton.disabled = true;
        $.get("query?user", result => {
            if (result) {
                let fields = JSON.parse(result)[0].fields;
                if (fields.ticks.indexOf(slackline.fields.uid) >= 0) {
                    this.sentButton.disabled = true;
                    this.sentButton.classList.add("btn-primary");
                    this.sentButton.classList.remove("btn-secondary");
                } else {
                    console.log("Enable!")
                    this.sentButton.disabled = false;
                }
            }
        });
        this.sentButton.addEventListener("click", event => {
            $.post("tick", {
                csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
                uid: slackline.fields.uid
            }, result => {
                this.sentButton.disabled = true;
                this.sentButton.classList.remove("btn-secondary");
                this.sentButton.classList.add("btn-primary");
            });
        })
        // Sharing Tools
        this.shareTools = this.toolbar.appendChild(document.createElement("div"))
        this.shareTools.className = "btn-group flex-grow-1";
        // Share Button
        this.shareButton = this.shareTools.appendChild(document.createElement("button"));
        this.shareButton.appendChild(iconSpan("share", true));
        this.shareButton.className = "btn btn-secondary";
        this.shareButton.addEventListener("click", event => {
            let dialog = slackline.share();
            if (this.parent) {
                this.parent.hide(true);
                dialog.xButton.addEventListener("click", event => {
                    this.parent.show()
                })
            }
        })
    }
}

class VotingBar {
    constructor(slackline) {
        this.slackline = slackline
        // Create Container
        this.div = document.createElement("div");
        this.div.className = "d-flex flex-row"
        // Progress Bar
        this.progress = this.div.appendChild(document.createElement("div"));
        this.progress.className = "progress flex-grow-1 mr-2";
        this.progress.style.height = "32px";
        // Upvote Bar
        this.upvoteBar = this.progress.appendChild(document.createElement("div"));
        this.upvoteBar.className = "progress-bar bg-success";
        addTooltip(this.upvoteBar, "Total Upvotes");
        // Downvote Bar
        this.downvoteBar = this.progress.appendChild(document.createElement("div"));
        this.downvoteBar.className = "progress-bar bg-danger";
        addTooltip(this.downvoteBar, "Total Downvotes");
        this.updateBar()
        // Voting Buttons
        this.buttonGroup = this.div.appendChild(document.createElement("div"));
        this.buttonGroup.className = "btn-group";
        // Upvote Button
        this.upvoteButton = this.buttonGroup.appendChild(document.createElement("button"));
        this.upvoteButton.appendChild(iconSpan("thumb_up"));
        this.upvoteButton.className = "btn btn-secondary"
        this.upvoteButton.disabled = true;
        addTooltip(this.upvoteButton, "Upvote");
        // Downvote Button
        this.downvoteButton = this.buttonGroup.appendChild(document.createElement("button"));
        this.downvoteButton.appendChild(iconSpan("thumb_down"));
        this.downvoteButton.className = "btn btn-secondary"
        this.downvoteButton.disabled = true;
        addTooltip(this.downvoteButton, "Downvote");
        // Behaviour
        $.get("query?user", result => {
            if (result) {
                let fields = JSON.parse(result)[0].fields;
                if (fields.upvotes.indexOf(slackline.fields.uid) >= 0 ||
                    fields.downvotes.indexOf(slackline.fields.uid) >= 0 ) {
                        this.upvoteButton.disabled = true;
                        this.downvoteButton.disabled = true;
                        if (fields.upvotes.indexOf(slackline.fields.uid)) {
                            this.upvoteButton.classList.remove("btn-secondary");
                            this.upvoteButton.classList.add("btn-success");
                        } else {
                            this.downvoteButton.classList.remove("btn-secondary");
                            this.downvoteButton.classList.add("btn-success");
                        }
                } else {
                    this.upvoteButton.disabled = false;
                    this.downvoteButton.disabled = false;
                }
            }
        });
        this.downvoteButton.addEventListener("click", event => {
            $.post("vote", {
                csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
                uid: slackline.fields.uid,
                downvote: ""
            }, result => {
                this.upvoteButton.disabled = true;
                this.downvoteButton.disabled = true;
                this.downvoteButton.classList.remove("btn-secondary");
                this.downvoteButton.classList.add("btn-danger");
                this.slackline.fields.downvotes += 1;
                this.updateBar()
            });
        });
        this.upvoteButton.addEventListener("click", event => {
            $.post("vote", {
                csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
                uid: slackline.fields.uid,
                upvote: ""
            }, result => {
                this.downvoteButton.disabled = true;
                this.upvoteButton.disabled = true;
                this.upvoteButton.classList.remove("btn-secondary");
                this.upvoteButton.classList.add("btn-success");
                this.slackline.fields.upvotes += 1;
                this.updateBar();
            });
        });
    }

    updateBar() {
        const totalVotes = this.slackline.fields.upvotes + this.slackline.fields.downvotes;
        const upvotePercentage = Math.round(this.slackline.fields.upvotes / totalVotes * 100);
        const downvotePercentage = 100 - upvotePercentage;
        if (totalVotes == 0) {
            this.upvoteBar.style.width = `50%`;
            this.downvoteBar.style.width = `50%`;
        } else {
            this.upvoteBar.style.width = `${upvotePercentage}%`;
            this.downvoteBar.style.width = `${downvotePercentage}%`;
        }
        this.upvoteBar.innerHTML = this.slackline.fields.upvotes;
        this.downvoteBar.innerHTML = this.slackline.fields.downvotes;
    }
}

class StatsBar {
    constructor(slackline) {
        this.slackline = slackline
        this.group = document.createElement("ul");
        this.group.className = "list-group list-group-horizontal justify-content-center";
        this.votes = this.group.appendChild(document.createElement("li"));
        this.votes.className = "list-group-item flex-grow-1";
        this.sends = this.group.appendChild(document.createElement("li"));
        this.sends.className = "list-group-item flex-grow-1";
        this.update();
    }
    
    update() {
        this.votes.innerHTML = `Total Votes: ${this.slackline.fields.upvotes + this.slackline.fields.downvotes}`;
        this.sends.innerHTML = `Total Sends: ${this.slackline.fields.ticks}`;
    }
}

class InfoDialog extends GenericDialog {
    constructor (slackline) {
        super();
        this.slackline = slackline;
        // Parameters
        this.mapPadding = 1/10000;
        // Set the URL
        history.pushState({}, null, `?uid=${slackline.fields.uid}`);
        // Title
        this.title = this.title.appendChild(new LineTitle(slackline).div);
        // Map
        this.mapDiv = this.body.appendChild(document.createElement("div"));
        this.mapDiv.style.width = "100%";
        this.mapDiv.style.height = "200px";
        this.mapDiv.style.backgroundColor = "black";
        this.map = new mapkit.Map(this.mapDiv);
        // Add Slackline to Map
        this.overlay = new mapkit.PolylineOverlay(
            this.slackline.anchors,
            {style: new mapkit.Style({
                strokeColor: this.slackline.color,
                lineWidth: 5,
                lineCap: "round"
            })}
        );
        this.annotation = new mapkit.Annotation(
            this.slackline.midpoint,
            this.slackline.factory,
            {
                data: this.slackline.fields,
            }
        );
        this.map.addAnnotation(this.annotation);
        this.map.addOverlay(this.overlay);
        // Move the Map's viewport to the Slackline
        this.map.mapType = mapkit.Map.MapTypes.Hybrid;
        let region = new mapkit.BoundingRegion(
            Math.max(...slackline.anchors.map(anchor => anchor.latitude)) + this.mapPadding,
            Math.max(...slackline.anchors.map(anchor => anchor.longitude)) + this.mapPadding,
            Math.min(...slackline.anchors.map(anchor => anchor.latitude)) - this.mapPadding,
            Math.min(...slackline.anchors.map(anchor => anchor.longitude)) - this.mapPadding,
        ).toCoordinateRegion();
        this.map.setRegionAnimated(region, false);
        map.setRegionAnimated(region, true);
        // Disallow interaction with the map.
        this.map.isRotationEnabled = false;
        this.map.isScrollEnabled = false;
        this.map.showsZoomControl = true;
        // Notes
        if (slackline.fields.notes) {
            this.notes = this.body.appendChild(document.createElement("div"));
            this.notes.className = "mt-2 list-group";
            this.notesField = this.notes.appendChild(document.createElement("div"));
            this.notesField.className = "list-group-item";
            this.notesLabel = this.notesField.appendChild(document.createElement("label"));
            this.notesLabel.className = "text-secondary mr-2 mb-0";
            this.notesLabel.innerHTML = "Notes:"
            this.notesField.innerHTML += slackline.fields.notes.replace(/\n/gi, "<br/>");
        }
        // Voting
        this.voting = this.body.appendChild(document.createElement("div"));
        this.voting.className = "mt-2";
        this.voting.appendChild(new VotingBar(slackline).div);
        // Stats
        this.stats = this.body.appendChild(document.createElement("div"));
        this.stats.className = "mt-2"
        this.stats.appendChild(new StatsBar(slackline).group);
        // Toolbar
        this.toolbar = new LineToolbar(slackline, this);
        this.footer.appendChild(this.toolbar.toolbar);
    }

    hide(temporary=false) {
        super.hide();
        if (!temporary) {
            this.map.destroy();
        }
    }

}