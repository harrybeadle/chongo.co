$('#account-button').click(function() {
    $('#account-modal').modal();
})


$('#activity-button').click(function() {
    ad.show()
})

//
// Registration Verification
//

var registerState = {};
function registerButton(field, state) {
    registerState[field] = state;
    var state = 0;
    for (var key in registerState) {
        state += registerState[key]
    }
    if (state) {
        $("#register-submit").prop("disabled", true);
        console.log("Bad Form!")
    } else {
        $("#register-submit").prop("disabled", false);
        console.log("Good Form!")
    }
}
function registerTakenHandle(field) {
    $.get("query?" + field + "=" + $('#register-' + field).val(),
        function(data) {
            if (data == '1') { // TAKEN
                $('#register-' + field + '-taken').css("display", "block")
                registerButton(field, true)
            } else { // NOT TAKEN
                $('#register-' + field + '-taken').css("display", "none")
                registerButton(field, false)
            }
        }
    );
}
$('#register-username').on('input', function() {registerTakenHandle("username")})
$('#register-email').on('input', function() {registerTakenHandle("email")})

function registerPasswordVerify() {
    if ($('#register-password').val() != $('#register-password-verify').val()) {
        $('#register-password-different').css("display", "block")
        registerButton("password", true)
    } else {
        $('#register-password-different').css("display", "none")
        registerButton("password", false)
    }
}
$('#register-password').on("input", registerPasswordVerify)
$('#register-password-verify').on("input", registerPasswordVerify)

//
// Cookies Consent
//

const cookies = Cookies.withAttributes({
    secure: true,
})

if (!cookies.get("consent")) {
    $('#cookie-modal').modal()
}
$('#cookie-accept').click(event => {
    $('#cookie-modal').modal('hide');
    cookies.set("consent", "Accepted");
})

//
// Tooltips
//

function addTooltip(element, tooltip) {
    $(element).tooltip({
        placement: "bottom",
        title: tooltip
    })
    element.addEventListener("click", event => {
        $(element).tooltip("hide")
    })
}

//
// Dialog
//

class GenericDialog {
    constructor() {
        // Modal
        this.modal = document.createElement("div")
        this.modal.className = "modal"
        // Dialog
        this.dialog = this.modal.appendChild(document.createElement("div"))
        this.dialog.className = "modal-dialog modal-dialog-centered modal-dialog-scrollable mx-auto "
        // Content
        this.content = this.dialog.appendChild(document.createElement("div"))
        this.content.className = "modal-content"
        // Header
        this.header = this.content.appendChild(document.createElement("div"))
        this.header.className = "modal-header"
        // Title
        this.title = this.header.appendChild(document.createElement("div"))
        this.title.className = "modal-title"
        // X Button
        this.xButton = this.header.appendChild(document.createElement("button"))
        this.xButton.className = "close"
        this.xButton.innerHTML = "&times;"
        this.xButton.addEventListener("click", event => {
            $(this.modal).modal("hide")
        })
        // Body
        this.body = this.content.appendChild(document.createElement("div"))
        this.body.className = "modal-body"
        // Buttons
        this.footer = this.content.appendChild(document.createElement("div"))
        this.footer.className = "modal-footer"
    }

    show(options={}) {
        $(this.modal).modal(options)
    }

    hide() {
        $(this.modal).modal("hide")
    }

}

function iconSpan(iconName, labelText=false) {
    let span = document.createElement("span");
    span.className = "icon";
    span.innerHTML = iconName;
    if (labelText) {
        let div = document.createElement("div");
        div.appendChild(span);
        let label = div.appendChild(document.createElement("span"));
        let labelString;
        if (labelText == true) {
            labelString = iconName.split("_")[0];
            labelString = labelString.charAt(0).toUpperCase() + labelString.slice(1);
        } else {
            labelString = labelText;
        }
        label.innerHTML = labelString;
        label.className = "ml-2 mr-1";
        return div;
    }
    return span;
}
