from django.db import models
from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User
from django.db.models.signals import pre_save

from datetime import datetime
import hashlib

CORD_MAX_DIGITS = 15
CORD_MAX_DECIMAL = 5

class LineType(models.TextChoices):
    LONGLINE = 'Longline'
    WATERLINE = 'Waterline'
    SLACKLINE = 'Slackline'
    HIGHLINE = 'Highline'

class Slackline(models.Model):
    uid = models.CharField(
        max_length=10
    )
    name = models.CharField(
        max_length=128,
        blank=True
    )
    owner = models.CharField(
        max_length=128
    )
    linetype = models.CharField(
        max_length=10,
        choices=LineType.choices,
        default=LineType.SLACKLINE
    )
    notes = models.CharField(
        max_length=2048,
        blank=True
    )
    coordinates = JSONField()
    length = models.IntegerField()
    upvotes = models.IntegerField(
        default=0
    )
    ticks = models.IntegerField(
        default=0
    )
    downvotes = models.IntegerField(
        default=0
    )
    created_on = models.DateTimeField(
        "Date Published",
        default=datetime.now, 
        blank=True
    )
    locality = models.CharField(
        max_length=50
    )
    country_code = models.CharField(
        max_length=2
    )
    def __str__(self):
        name = ""
        if self.name: name = f"'{self.name}'"
        return f"{self.length}m {self.linetype} {name} by {self.owner}"

def gen_uid(sender, instance, using, **kwargs):
    instance.uid = hashlib.sha256(str(instance.coordinates).encode('utf-8')).hexdigest()[:10]
pre_save.connect(gen_uid, sender=Slackline)

class Slacker(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    upvotes = JSONField(default=list)
    downvotes = JSONField(default=list)
    ticks = JSONField(default=list)